﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace MushroomClassification {
    public partial class Form1 : Form {

        ShroomClassificator shroomClassificator;
        string[,] shroomParameters;

        public Form1() {
            InitializeComponent();

            initializeUI();
            initializeDropDownMenus();
            initializeProgressBar(0.5);
        }

        private void initializeProgressBar(double valueOfEdible) {
            if (valueOfEdible > 1) {
                lbl_edible.ForeColor = System.Drawing.Color.Green;
                lbl_notEdible.ForeColor = SystemColors.ControlText;
                lbl_yesNoEdible.ForeColor = SystemColors.ControlText;

                pb_notEdibleBar.Value = 0;
                pb_isEdibleBar.Value = (int)(50);
                return;
            }

            if (valueOfEdible*100 == 50) {
                lbl_edible.ForeColor = SystemColors.ControlText;
                lbl_notEdible.ForeColor = SystemColors.ControlText;
                pb_notEdibleBar.Value = 0;
                pb_isEdibleBar.Value = 0;
                lbl_yesNoEdible.ForeColor = System.Drawing.Color.YellowGreen;
            } else if (valueOfEdible*100 < 50) {
                lbl_edible.ForeColor = System.Drawing.Color.Green;
                lbl_notEdible.ForeColor = SystemColors.ControlText;
                lbl_yesNoEdible.ForeColor = SystemColors.ControlText;             

                pb_notEdibleBar.Value = 0;
                pb_isEdibleBar.Value = (int)(50 - valueOfEdible * 100);
            } else if (valueOfEdible*100 > 50) {
                lbl_notEdible.ForeColor = System.Drawing.Color.Red;
                lbl_edible.ForeColor = SystemColors.ControlText;
                lbl_yesNoEdible.ForeColor = SystemColors.ControlText;

                pb_isEdibleBar.Value = 0;
                pb_notEdibleBar.Value = (int)(valueOfEdible * 100-50);
            } 
        }

        private void initializeDropDownMenus() {
            var capShape = new List<ShroomType>();
            capShape.Add(new ShroomType() { Name = "bell", Value = "b" });
            capShape.Add(new ShroomType() { Name = "conical", Value = "c" });
            capShape.Add(new ShroomType() { Name = "convex", Value = "x" });
            capShape.Add(new ShroomType() { Name = "flat", Value = "f" });
            capShape.Add(new ShroomType() { Name = "knobbed", Value = "k" });
            capShape.Add(new ShroomType() { Name = "sunken", Value = "s" });
            capShape.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_capShape.DataSource = capShape;
            cb_capShape.DisplayMember = "Name";
            cb_capShape.ValueMember = "Value";
            cb_capShape.DropDownStyle = ComboBoxStyle.DropDownList;

            var capSurface = new List<ShroomType>();
            capSurface.Add(new ShroomType() { Name = "fibrous", Value = "f" });
            capSurface.Add(new ShroomType() { Name = "grooves", Value = "g" });
            capSurface.Add(new ShroomType() { Name = "scaly", Value = "y" });
            capSurface.Add(new ShroomType() { Name = "smooth", Value = "s" });
            capSurface.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_capSurface.DataSource = capSurface;
            cb_capSurface.DisplayMember = "Name";
            cb_capSurface.ValueMember = "Value";
            cb_capSurface.DropDownStyle = ComboBoxStyle.DropDownList;

            var capColor = new List<ShroomType>();
            capColor.Add(new ShroomType() { Name = "brown", Value = "n" });
            capColor.Add(new ShroomType() { Name = "buff", Value = "b" });
            capColor.Add(new ShroomType() { Name = "cinnamon", Value = "c" });
            capColor.Add(new ShroomType() { Name = "gray", Value = "g" });
            capColor.Add(new ShroomType() { Name = "pink", Value = "p" });
            capColor.Add(new ShroomType() { Name = "purple", Value = "u" });
            capColor.Add(new ShroomType() { Name = "red", Value = "e" });
            capColor.Add(new ShroomType() { Name = "white", Value = "w" });
            capColor.Add(new ShroomType() { Name = "yellow", Value = "y" });
            capColor.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_capColor.DataSource = capColor;
            cb_capColor.DisplayMember = "Name";
            cb_capColor.ValueMember = "Value";
            cb_capColor.DropDownStyle = ComboBoxStyle.DropDownList;

            var bruises = new List<ShroomType>();
            bruises.Add(new ShroomType() { Name = "bruises", Value = "t" });
            bruises.Add(new ShroomType() { Name = "no", Value = "f" });
            bruises.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_bruises.DataSource = bruises;
            cb_bruises.DisplayMember = "Name";
            cb_bruises.ValueMember = "Value";
            cb_bruises.DropDownStyle = ComboBoxStyle.DropDownList;

            var odor = new List<ShroomType>();
            odor.Add(new ShroomType() { Name = "almond", Value = "a" });
            odor.Add(new ShroomType() { Name = "anise", Value = "l" });
            odor.Add(new ShroomType() { Name = "creosote", Value = "c" });
            odor.Add(new ShroomType() { Name = "fishy", Value = "y" });
            odor.Add(new ShroomType() { Name = "foul", Value = "f" });
            odor.Add(new ShroomType() { Name = "musty", Value = "m" });
            odor.Add(new ShroomType() { Name = "none", Value = "n" });
            odor.Add(new ShroomType() { Name = "pungent", Value = "p" });
            odor.Add(new ShroomType() { Name = "spicy", Value = "s" });
            odor.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_odor.DataSource = odor;
            cb_odor.DisplayMember = "Name";
            cb_odor.ValueMember = "Value";
            cb_odor.DropDownStyle = ComboBoxStyle.DropDownList;

            var gillAttachment = new List<ShroomType>();
            gillAttachment.Add(new ShroomType() { Name = "attached", Value = "a" });
            gillAttachment.Add(new ShroomType() { Name = "descending", Value = "d" });
            gillAttachment.Add(new ShroomType() { Name = "free", Value = "f" });
            gillAttachment.Add(new ShroomType() { Name = "notched", Value = "n" });
            gillAttachment.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_gillAttachment.DataSource = gillAttachment;
            cb_gillAttachment.DisplayMember = "Name";
            cb_gillAttachment.ValueMember = "Value";
            cb_gillAttachment.DropDownStyle = ComboBoxStyle.DropDownList;

            var gillSpacing = new List<ShroomType>();
            gillSpacing.Add(new ShroomType() { Name = "close", Value = "c" });
            gillSpacing.Add(new ShroomType() { Name = "crowded", Value = "w" });
            gillSpacing.Add(new ShroomType() { Name = "distant", Value = "d" });
            gillSpacing.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_gillSpacing.DataSource = gillSpacing;
            cb_gillSpacing.DisplayMember = "Name";
            cb_gillSpacing.ValueMember = "Value";
            cb_gillSpacing.DropDownStyle = ComboBoxStyle.DropDownList;

            var gillSize = new List<ShroomType>();
            gillSize.Add(new ShroomType() { Name = "broad", Value = "b" });
            gillSize.Add(new ShroomType() { Name = "narrow", Value = "n" });
            gillSize.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_gillSize.DataSource = gillSize;
            cb_gillSize.DisplayMember = "Name";
            cb_gillSize.ValueMember = "Value";
            cb_gillSize.DropDownStyle = ComboBoxStyle.DropDownList;

            var gillColor = new List<ShroomType>();
            gillColor.Add(new ShroomType() { Name = "black", Value = "k" });
            gillColor.Add(new ShroomType() { Name = "brown", Value = "n" });
            gillColor.Add(new ShroomType() { Name = "buff", Value = "b" });
            gillColor.Add(new ShroomType() { Name = "chocolate", Value = "h" });
            gillColor.Add(new ShroomType() { Name = "gray", Value = "g" });
            gillColor.Add(new ShroomType() { Name = "green", Value = "r" });
            gillColor.Add(new ShroomType() { Name = "orange", Value = "o" });
            gillColor.Add(new ShroomType() { Name = "pink", Value = "p" });
            gillColor.Add(new ShroomType() { Name = "purple", Value = "u" });
            gillColor.Add(new ShroomType() { Name = "red", Value = "e" });
            gillColor.Add(new ShroomType() { Name = "white", Value = "w" });
            gillColor.Add(new ShroomType() { Name = "yellow", Value = "y" });
            gillColor.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_gillColor.DataSource = gillColor;
            cb_gillColor.DisplayMember = "Name";
            cb_gillColor.ValueMember = "Value";
            cb_gillColor.DropDownStyle = ComboBoxStyle.DropDownList;

            var stalkShape = new List<ShroomType>();
            stalkShape.Add(new ShroomType() { Name = "enlarging", Value = "e" });
            stalkShape.Add(new ShroomType() { Name = "tapering", Value = "t" });
            stalkShape.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_stalkShape.DataSource = stalkShape;
            cb_stalkShape.DisplayMember = "Name";
            cb_stalkShape.ValueMember = "Value";
            cb_stalkShape.DropDownStyle = ComboBoxStyle.DropDownList;

            var stalkRoot = new List<ShroomType>();
            stalkRoot.Add(new ShroomType() { Name = "bulbous", Value = "b" });
            stalkRoot.Add(new ShroomType() { Name = "club", Value = "c" });
            stalkRoot.Add(new ShroomType() { Name = "cup", Value = "u" });
            stalkRoot.Add(new ShroomType() { Name = "equal", Value = "e" });
            stalkRoot.Add(new ShroomType() { Name = "rhizomorphs", Value = "z" });
            stalkRoot.Add(new ShroomType() { Name = "rooted", Value = "r" });
            stalkRoot.Add(new ShroomType() { Name = "missing", Value = "?" });
            stalkRoot.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_stalkRoot.DataSource = stalkRoot;
            cb_stalkRoot.DisplayMember = "Name";
            cb_stalkRoot.ValueMember = "Value";
            cb_stalkRoot.DropDownStyle = ComboBoxStyle.DropDownList;

            var StalkSurAbove = new List<ShroomType>();
            StalkSurAbove.Add(new ShroomType() { Name = "fibrous", Value = "f" });
            StalkSurAbove.Add(new ShroomType() { Name = "scaly", Value = "y" });
            StalkSurAbove.Add(new ShroomType() { Name = "silky", Value = "k" });
            StalkSurAbove.Add(new ShroomType() { Name = "smooth", Value = "s" });
            StalkSurAbove.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_StalkSurAbove.DataSource = StalkSurAbove;
            cb_StalkSurAbove.DisplayMember = "Name";
            cb_StalkSurAbove.ValueMember = "Value";
            cb_StalkSurAbove.DropDownStyle = ComboBoxStyle.DropDownList;

            var stalkSurBelow = new List<ShroomType>();
            stalkSurBelow.Add(new ShroomType() { Name = "fibrous", Value = "f" });
            stalkSurBelow.Add(new ShroomType() { Name = "scaly", Value = "y" });
            stalkSurBelow.Add(new ShroomType() { Name = "silky", Value = "k" });
            stalkSurBelow.Add(new ShroomType() { Name = "smooth", Value = "s" });
            stalkSurBelow.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_stalkSurBelow.DataSource = stalkSurBelow;
            cb_stalkSurBelow.DisplayMember = "Name";
            cb_stalkSurBelow.ValueMember = "Value";
            cb_stalkSurBelow.DropDownStyle = ComboBoxStyle.DropDownList;

            var stalkColAbove = new List<ShroomType>();
            stalkColAbove.Add(new ShroomType() { Name = "brown", Value = "n" });
            stalkColAbove.Add(new ShroomType() { Name = "buff", Value = "b" });
            stalkColAbove.Add(new ShroomType() { Name = "cinnamon", Value = "c" });
            stalkColAbove.Add(new ShroomType() { Name = "gray", Value = "g" });
            stalkColAbove.Add(new ShroomType() { Name = "orange", Value = "o" });
            stalkColAbove.Add(new ShroomType() { Name = "pink", Value = "p" });
            stalkColAbove.Add(new ShroomType() { Name = "red", Value = "e" });
            stalkColAbove.Add(new ShroomType() { Name = "white", Value = "w" });
            stalkColAbove.Add(new ShroomType() { Name = "yellow", Value = "y" });
            stalkColAbove.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_stalkColAbove.DataSource = stalkColAbove;
            cb_stalkColAbove.DisplayMember = "Name";
            cb_stalkColAbove.ValueMember = "Value";
            cb_stalkColAbove.DropDownStyle = ComboBoxStyle.DropDownList;

            var stalkColBelow = new List<ShroomType>();
            stalkColBelow.Add(new ShroomType() { Name = "brown", Value = "n" });
            stalkColBelow.Add(new ShroomType() { Name = "buff", Value = "b" });
            stalkColBelow.Add(new ShroomType() { Name = "cinnamon", Value = "c" });
            stalkColBelow.Add(new ShroomType() { Name = "gray", Value = "g" });
            stalkColBelow.Add(new ShroomType() { Name = "orange", Value = "o" });
            stalkColBelow.Add(new ShroomType() { Name = "pink", Value = "p" });
            stalkColBelow.Add(new ShroomType() { Name = "red", Value = "e" });
            stalkColBelow.Add(new ShroomType() { Name = "white", Value = "w" });
            stalkColBelow.Add(new ShroomType() { Name = "yellow", Value = "y" });
            stalkColBelow.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_stalkColBelow.DataSource = stalkColBelow;
            cb_stalkColBelow.DisplayMember = "Name";
            cb_stalkColBelow.ValueMember = "Value";
            cb_stalkColBelow.DropDownStyle = ComboBoxStyle.DropDownList;

            var veilType = new List<ShroomType>();
            veilType.Add(new ShroomType() { Name = "partial", Value = "p" });
            veilType.Add(new ShroomType() { Name = "universal", Value = "u" });
            veilType.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_veilType.DataSource = veilType;
            cb_veilType.DisplayMember = "Name";
            cb_veilType.ValueMember = "Value";
            cb_veilType.DropDownStyle = ComboBoxStyle.DropDownList;

            var veilColor = new List<ShroomType>();
            veilColor.Add(new ShroomType() { Name = "brown", Value = "n" });
            veilColor.Add(new ShroomType() { Name = "orange", Value = "o" });
            veilColor.Add(new ShroomType() { Name = "white", Value = "w" });
            veilColor.Add(new ShroomType() { Name = "yellow", Value = "y" });
            veilColor.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_veilColor.DataSource = veilColor;
            cb_veilColor.DisplayMember = "Name";
            cb_veilColor.ValueMember = "Value";
            cb_veilColor.DropDownStyle = ComboBoxStyle.DropDownList;

            var ringNumber = new List<ShroomType>();
            ringNumber.Add(new ShroomType() { Name = "none", Value = "n" });
            ringNumber.Add(new ShroomType() { Name = "one", Value = "o" });
            ringNumber.Add(new ShroomType() { Name = "two", Value = "t" });
            ringNumber.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_ringNumber.DataSource = ringNumber;
            cb_ringNumber.DisplayMember = "Name";
            cb_ringNumber.ValueMember = "Value";
            cb_ringNumber.DropDownStyle = ComboBoxStyle.DropDownList;

            var ringType = new List<ShroomType>();
            ringType.Add(new ShroomType() { Name = "cobwebby", Value = "c" });
            ringType.Add(new ShroomType() { Name = "evanescent", Value = "e" });
            ringType.Add(new ShroomType() { Name = "flaring", Value = "f" });
            ringType.Add(new ShroomType() { Name = "large", Value = "l" });
            ringType.Add(new ShroomType() { Name = "none", Value = "n" });
            ringType.Add(new ShroomType() { Name = "pendant", Value = "p" });
            ringType.Add(new ShroomType() { Name = "sheathing", Value = "s" });
            ringType.Add(new ShroomType() { Name = "zone", Value = "z" });
            ringType.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_ringType.DataSource = ringType;
            cb_ringType.DisplayMember = "Name";
            cb_ringType.ValueMember = "Value";
            cb_ringType.DropDownStyle = ComboBoxStyle.DropDownList;

            var sporePintCol = new List<ShroomType>();
            sporePintCol.Add(new ShroomType() { Name = "black", Value = "k" });
            sporePintCol.Add(new ShroomType() { Name = "brown", Value = "n" });
            sporePintCol.Add(new ShroomType() { Name = "buff", Value = "b" });
            sporePintCol.Add(new ShroomType() { Name = "chocolate", Value = "h" });
            sporePintCol.Add(new ShroomType() { Name = "green", Value = "r" });
            sporePintCol.Add(new ShroomType() { Name = "orange", Value = "o" });
            sporePintCol.Add(new ShroomType() { Name = "purple", Value = "u" });
            sporePintCol.Add(new ShroomType() { Name = "white", Value = "w" });
            sporePintCol.Add(new ShroomType() { Name = "yellow", Value = "y" });
            sporePintCol.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_sporePintCol.DataSource = sporePintCol;
            cb_sporePintCol.DisplayMember = "Name";
            cb_sporePintCol.ValueMember = "Value";
            cb_sporePintCol.DropDownStyle = ComboBoxStyle.DropDownList;

            var population = new List<ShroomType>();
            population.Add(new ShroomType() { Name = "abundant", Value = "a" });
            population.Add(new ShroomType() { Name = "clustered", Value = "c" });
            population.Add(new ShroomType() { Name = "numerous", Value = "n" });
            population.Add(new ShroomType() { Name = "scattered", Value = "s" });
            population.Add(new ShroomType() { Name = "several", Value = "v" });
            population.Add(new ShroomType() { Name = "solitary", Value = "y" });
            population.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_population.DataSource = population;
            cb_population.DisplayMember = "Name";
            cb_population.ValueMember = "Value";
            cb_population.DropDownStyle = ComboBoxStyle.DropDownList;

            var habitat = new List<ShroomType>();
            habitat.Add(new ShroomType() { Name = "grasses", Value = "g" });
            habitat.Add(new ShroomType() { Name = "leaves", Value = "l" });
            habitat.Add(new ShroomType() { Name = "meadows", Value = "m" });
            habitat.Add(new ShroomType() { Name = "paths", Value = "p" });
            habitat.Add(new ShroomType() { Name = "urban", Value = "u" });
            habitat.Add(new ShroomType() { Name = "waste", Value = "w" });
            habitat.Add(new ShroomType() { Name = "woods", Value = "d" });
            habitat.Add(new ShroomType() { Name = "not sure", Value = "?" });
            cb_habitat.DataSource = habitat;
            cb_habitat.DisplayMember = "Name";
            cb_habitat.ValueMember = "Value";
            cb_habitat.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        private void initializeUI() {
            btn_predict.Enabled = false;
            btn_errors.Enabled = false;

            shroomClassificator = new ShroomClassificator();

            pb_isEdibleBar.Maximum = 50;
            pb_notEdibleBar.Maximum = 50;
            //Task: napraviti custom progressBar, zbog drugih boja
            pb_isEdibleBar.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            pb_isEdibleBar.RightToLeftLayout = true;
            pb_notEdibleBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            pb_notEdibleBar.RightToLeftLayout = false;
        }

        private void btn_predict_Click(object sender, System.EventArgs e) {float result;
            result = float.Parse(shroomClassificator.getResults()[1], CultureInfo.InvariantCulture.NumberFormat);
            initializeProgressBar(result);
        }

        private void getValuesFromDropMenus() {
            shroomParameters = new string[,] { { "$",
                    cb_capShape.SelectedValue.ToString(),
                    cb_capSurface.SelectedValue.ToString(),
                    cb_capColor.SelectedValue.ToString(),
                    cb_bruises.SelectedValue.ToString(),
                    cb_odor.SelectedValue.ToString(),
                    cb_gillAttachment.SelectedValue.ToString(),
                    cb_gillSpacing.SelectedValue.ToString(),
                    cb_gillSize.SelectedValue.ToString(),
                    cb_gillColor.SelectedValue.ToString(),
                    cb_stalkShape.SelectedValue.ToString(),
                    cb_stalkRoot.SelectedValue.ToString(),
                    cb_StalkSurAbove.SelectedValue.ToString(),
                    cb_stalkSurBelow.SelectedValue.ToString(),
                    cb_stalkColAbove.SelectedValue.ToString(),
                    cb_stalkColBelow.SelectedValue.ToString(),
                    cb_veilType.SelectedValue.ToString(),
                    cb_veilColor.SelectedValue.ToString(),
                    cb_ringNumber.SelectedValue.ToString(),
                    cb_ringType.SelectedValue.ToString(),
                    cb_sporePintCol.SelectedValue.ToString(),
                    cb_population.SelectedValue.ToString(),
                    cb_habitat.SelectedValue.ToString() } };
        }

        private void btn_startService_Click(object sender, System.EventArgs e) {
            getValuesFromDropMenus();

            shroomClassificator.setParameters(shroomParameters);
            shroomClassificator.StartPredictionService();

            if (shroomClassificator.getResultsError() != null) {
                btn_errors.Enabled = true;
                MessageBox.Show(shroomClassificator.getResultsError());
            }

            MessageBox.Show("Connection successful! You can now Predict!");
            btn_predict.Enabled = true;
        }

        private void dropDownElementClicked(object sender, EventArgs e) {
            btn_predict.Enabled = false;
            btn_errors.Enabled = true;
            initializeProgressBar(0.5);
        }
    }
}