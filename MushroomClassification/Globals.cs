﻿namespace MushroomClassification {
    public class StringTable {
        public string[] ColumnNames { get; set; }
        public string[,] Values { get; set; }
    }

    public class ShroomType {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
